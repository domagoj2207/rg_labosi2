#ifdef _WIN32
#include <windows.h>             //bit ce ukljuceno ako se koriste windows
#endif

#define _USE_MATH_DEFINES
//  #include <GL/Gl.h>
//  #include <GL/Glu.h>    nije potrebno ako se koristi glut
#include <GL/glut.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <fstream>
#include <vector>
#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <glm/glm.hpp> //glm::vec3...
#include <iomanip>
#include <time.h>

using namespace std;

typedef struct {
	double x;
	double y;
	double z;
}kordinateVrha;

typedef struct {
	double x, y, z; //pozicija cestice
	double r, g, b; //komponente boje
	double faktor_kretnje; //faktor kretanja zadanim putem (putanjom)
	double sx, sy, sz; //put
	double osx, osy, osz; //promjena orijentacije poligona
	double kut;
	int t; //vrijeme zivljenja
}cestica;

//GLOBALNNE varijable
GLuint window;
GLuint sub_width = 800, sub_height = 600;
GLuint tekstura;
int kut = 0;
int dimnjacar = 0;
int staro_t = 0;
vector<cestica> sustav_cestica;
//orijentacija vektora je zadana sa s=(0,0,1), e=orijentacija koju zelimo postic
//****************************************************************
//	Function Prototypes.
//****************************************************************
void myDisplay();
void myReshape(int width, int height);
GLuint ucitajTeksturu(const char *pic); //buduci da mi nisu jasni bili predlozeni nacini uzeta je impl s interneta
void mojaFunkcija();
void iscrtajSustavCestica();
void iscrtajCesticu(cestica);
vector<double> pocetne_orijentacije();
vector<double> promjeni_boju();
//******************************************************************
//	Glavni program.
//******************************************************************
int main(int argc, char ** argv)
{
	std::ifstream in("cestica.bmp");
	if (!(in.is_open())) {
		printf_s("Cannot open input file.\n");
		exit(1);
	}
	//datoteka je ispravna
	tekstura = ucitajTeksturu("cestica.bmp");
	srand(time(NULL));

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(sub_width, sub_height);
	glutInitWindowPosition(300, 100);
	glutInit(&argc, argv);
	window = glutCreateWindow("labos RG");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutIdleFunc(mojaFunkcija);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, tekstura);

	glutMainLoop();
	return 0;
}
//*********************************************************************************
//	Promjena velicine prozora.
//*********************************************************************************
void myReshape(int width, int height){

	sub_width = width;                      	//promjena sirine prozora
	sub_height = height;						//promjena visine prozora
	glViewport(0, 0, sub_width, sub_height);	//  otvor u prozoru
	glMatrixMode(GL_PROJECTION);                // Select The Projection Matrix
	glLoadIdentity();                           // Reset The Projection Matrix
	// ova metoda nejasna
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	//ja sam imao gluOrtho2D(0,width,0,height);
	glMatrixMode(GL_MODELVIEW);                 // Select The Modelview Matrix
	glLoadIdentity();							//	jedinicna matrica
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);		//	boja pozadine ; alfa = 0 =>prozirno
	glClear(GL_COLOR_BUFFER_BIT);				//	brisanje pozadine
	glPointSize(0.5);							//	postavi velicinu tocke za liniju
	glColor3f(0.0f, 0.0f, 0.0f);				//	postavi boju linije
	/*
	sub_width = width;                      	// zapamti novu sirinu prozora
	sub_height = height;				// zapamti novu visinu prozora
	glViewport(0, 0, sub_width, sub_height);	// otvor u prozoru
	glMatrixMode(GL_PROJECTION);			// matrica projekcije
	glLoadIdentity();				// jedinicna matrica
	*/
}
//*********************************************************************************
//	Osvjezavanje prikaza. (nakon preklapanja prozora) 
//*********************************************************************************
void myDisplay(){

	glLoadIdentity();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glTranslatef(-10.0, 0.0, -100.0);
	//glScalef(scaleX, scaleY, scaleZ);
	iscrtajSustavCestica();
	glutSwapBuffers();
	glFlush();
}
	
void mojaFunkcija() {

	int time;
	float dt; //Delta time u sekundama
	time = glutGet(GLUT_ELAPSED_TIME); //vrati broj milisekundi
	dt = (time - staro_t);
	if (dt > 50) { //slika neka se osvjezava svako 20ms kako bi u minuti potencijalno imali 20fps
		dimnjacar++;
		int nove_cestice = rand() % 6  + 10; // svaki krug se stvara 10 do 15 novih cestica 
		if (dimnjacar > 50) {
			nove_cestice = rand() % 21 + 150; //efekt dima 10 prolazaka
			if (dimnjacar > 60 )
				dimnjacar = 0;
		}
		for (int i = 0; i < nove_cestice; i++) { //stoga po sekundi se stvara u prosjeku 1000 cestica
			cestica cestica_i;
			vector<double> orijentacije; //sve su pocetno na istoj tocki radanja i sve su pocetno bijele boje
			if (i % 2 == 0) {
				cestica_i.x = 5.0;
				cestica_i.y = 5.0;
				cestica_i.z = 5.0;
			}
			else {
				cestica_i.x = 0.0;
				cestica_i.y = 0.0;
				cestica_i.z = 0.0;
			}

			cestica_i.r = 1.0;
			cestica_i.g = 1.0;
			cestica_i.b = 1.0;
			orijentacije = pocetne_orijentacije();
			cestica_i.sx = orijentacije[0];
			cestica_i.sy = orijentacije[1];
			cestica_i.sz = orijentacije[2];
			cestica_i.faktor_kretnje = 0.25 + ((double)(rand() % 11) / 20.0); //faktor kretanja putem od 0.25 do 0.75
			cestica_i.t = 45 + (rand()%26); //svaka je ziva izmedu 45 i 70 prolazaka tj 
			sustav_cestica.push_back(cestica_i);
		}
		//promjena orijentacije poligona - prethodni labos
		int broj_cestica = sustav_cestica.size();
		printf("broj cestica %d\n", broj_cestica);
		for (int j = 0; j < broj_cestica; j++) {
			kordinateVrha s = { 0.0,0.0,1.0 }; //pocetna orijentacija vektora = s
			kordinateVrha e = { 0.0,0.0,0.0 }; 
			kordinateVrha os = { 0.0,0.0,0.0 };
			// orijentacija koju zelimo postici = e
			e.x = sustav_cestica.at(j).x;//posto je ociste za x i y =0
			e.y = sustav_cestica.at(j).y; 
			e.z = sustav_cestica.at(j).z - 100.0;
			//odreduje os oko koje je potrebno rotirati objekt
			os.x = s.y*e.z - e.y*s.z;
			os.y = e.x*s.z - s.x*e.z;
			os.z = s.x*e.y - s.y*e.x;
			double module_s = sqrt(pow(s.x, 2) + pow(s.y, 2) + pow(s.z, 2));
			double module_e = sqrt(pow(e.x, 2) + pow(e.y, 2) + pow(e.z, 2));
			double kut = (s.x*e.x + s.y*e.y + s.z*e.z) / (module_s*module_e);
			kut = kut * 180 / M_PI;
			sustav_cestica.at(j).kut = kut;
			sustav_cestica.at(j).osx = os.x;
			sustav_cestica.at(j).osy = os.y;
			sustav_cestica.at(j).osz = os.z;
			//izmjena boje u r, g, b, r+g, r+b, g+b, white
 			vector<double> nova_boja = promjeni_boju();
			sustav_cestica.at(j).r = nova_boja[0];
			sustav_cestica.at(j).g = nova_boja[1];
			sustav_cestica.at(j).b = nova_boja[2];
			//postavljanje trenutne pozicije
			sustav_cestica.at(j).x += sustav_cestica.at(j).faktor_kretnje * sustav_cestica.at(j).sx;
			sustav_cestica.at(j).y += sustav_cestica.at(j).faktor_kretnje * sustav_cestica.at(j).sy;
			sustav_cestica.at(j).z += sustav_cestica.at(j).faktor_kretnje * sustav_cestica.at(j).sz;
			sustav_cestica.at(j).t -= 1; //"mrtvljenje"
			
		}
		int k = sustav_cestica.size()-1;
		while (k>=0) {
			if (sustav_cestica.at(k).t == 0) {
				sustav_cestica.erase(sustav_cestica.begin() + k);
				//printf("	Mrttva  ");
			}
			k--;
		}
		myDisplay();
		staro_t = time;
	}
}

vector<double> promjeni_boju() {
	vector<double> boja;
	int random_boja = rand() % 7;
	if (random_boja == 0) { //crvena
		boja.push_back(1.0);
		boja.push_back(0.0);
		boja.push_back(0.0);
		return boja;
	}
	else if (random_boja == 1) { //zelena
		boja.push_back(0.0);
		boja.push_back(1.0);
		boja.push_back(0.0);
		return boja;
	}
	else if (random_boja == 2) { //plava
		boja.push_back(0.0);
		boja.push_back(0.0);
		boja.push_back(1.0);
		return boja;
	}
	else if (random_boja == 3) { //crvena+zelena
		boja.push_back(1.0);
		boja.push_back(1.0);
		boja.push_back(0.0);
		return boja;
	}
	else if (random_boja == 4) { //crvena+plava
		boja.push_back(1.0);
		boja.push_back(0.0);
		boja.push_back(1.0);
		return boja;
	}
	else if (random_boja == 5) { //zelena+plava
		boja.push_back(0.0);
		boja.push_back(1.0);
		boja.push_back(1.0);
		return boja;
	}//bijela
	boja.push_back(1.0);
	boja.push_back(1.0);
	boja.push_back(1.0);
	return boja;
}

vector<double> pocetne_orijentacije() {
	vector<double> rezultat_pomaka;
	rezultat_pomaka.push_back((rand() % 2001 - 1000) / 1000.0); //sx
	rezultat_pomaka.push_back((rand() % 2001 - 1000) / 1000.0); //sy
	rezultat_pomaka.push_back((rand() % 2001 - 1000) / 1000.0); //sz
	return rezultat_pomaka;
}

//crtanje http://www.cs.cornell.edu/courses/cs4620/2011fa/lectures/practicum05.pdf
void iscrtajSustavCestica() {
	//printf("   iscrtavaj   ");
	for (int i = 0; i < sustav_cestica.size(); i++) {
		glColor3f(sustav_cestica.at(i).r, sustav_cestica.at(i).g, sustav_cestica.at(i).b);
		glTranslatef(sustav_cestica.at(i).x, sustav_cestica.at(i).y, sustav_cestica.at(i).z);
		glRotatef(sustav_cestica.at(i).kut, sustav_cestica.at(i).osx, sustav_cestica.at(i).osy, sustav_cestica.at(i).osz);
		glBegin(GL_QUADS);
		glTexCoord2f(1, 1);
		glVertex3f(0.5, 0.5, 0.0);
		glTexCoord2f(0, 1);
		glVertex3f(-0.5, 0.5, 0.0);
		glTexCoord2f(0, 0);
		glVertex3f(-0.5, -0.5, 0.0);
		glTexCoord2f(1, 0);
		glVertex3f(0.5, -0.5, 0.0);	
		glEnd();
		glRotatef(-sustav_cestica.at(i).kut, sustav_cestica.at(i).osx, sustav_cestica.at(i).osy, sustav_cestica.at(i).osz);
		glTranslatef(-sustav_cestica.at(i).x, -sustav_cestica.at(i).y, -sustav_cestica.at(i).z);
	}

}

//ideja je koristena sa stranice: http://www.cplusplus.com/forum/windows/78897/
//,sa stranice: https://stackoverflow.com/questions/12518111/how-to-load-a-bmp-on-glut-to-use-it-as-a-texture
//i sa stranice: https://www.khronos.org/opengl/wiki/Common_Mistakes
GLuint ucitajTeksturu(const char *pic){
	int w, h; 
	unsigned char *data;
	FILE *picfile;
	picfile = fopen(pic, "rb");
	if (picfile == NULL)
		return 0;
	//određivanje visine i sirine
	BITMAPINFOHEADER MyHeaderInfo;
	fread(&MyHeaderInfo, sizeof(BITMAPINFOHEADER), 1, picfile);
	w = MyHeaderInfo.biWidth; 
	h = MyHeaderInfo.biHeight;
	//zauzimanje memorije
	data = (unsigned char*)malloc(w*h*3);
	fread(data, w*h * 3, 1, picfile);
	fclose(picfile);
	//reverse-anje boje iz bgr u rgb
	for (int i = 0; i < w*h; i++){
		int index = i * 3;
		unsigned char B, R;
		B = data[index];
		R = data[index + 2];
		data[index] = R;
		data[index + 2] = B;
	}
	GLuint pom_tekstura;
	glGenTextures(1, &pom_tekstura); //generiranje teksture
	glBindTexture(GL_TEXTURE_2D, pom_tekstura); //privremeno bindanje te teksture
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); //postavljanje parametra teksture za wrapping
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST); //postavljanje parametra teksture za filtriranje
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, data);
	free(data);
	return pom_tekstura;
}
